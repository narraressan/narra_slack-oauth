const express = require('express');
const request = require('request');
const settings = {
	client_id : '313599189861.446915671268',
	client_secret: '56f4c656bd16dfab858b38e8653a3c4c',
	redirect_uri : 'http://lvh.me:3000/auth/slack/callback',
	scope: 'incoming-webhook'
};

const app = express()
app.use(express.static('./'));
app.use(require('body-parser').urlencoded({ extended: true }))


app.get('/', function(req, res){
	res.render('/index.html')
})


app.get('/register', function(req, res){
	res.redirect('https://slack.com/oauth/authorize?client_id=313599189861.446915671268&scope=incoming-webhook')
})


app.get('/auth/slack/callback', function(req, res, next) {
	if(typeof req.query.code != 'undefined'){
		let options = {
			method: 'POST',
			url: 'https://slack.com/api/oauth.access',
			headers: {
				'content-type' : 'application/x-www-form-urlencoded',
			},
			form: {
				client_id: settings.client_id,
				client_secret: settings.client_secret,
				scope: settings.scope,
				code: req.query.code,
				redirect_uri: settings.redirect_uri
			}
		};
		request(options, function(error, response, body){
			if(error){ res.json({ error: 'auth-failed' }) }
			if(response){
				try{ res.json(JSON.parse(body)); }
				catch(error){ res.json({ error: 'parsing-failed' }); }
			}
			else{ res.json({ error: 'response-failed' }); }
		});
	}
	else { res.json({ error: 'code-failed' }) }
});


app.listen(3000, function(){ console.log('API running...') })
